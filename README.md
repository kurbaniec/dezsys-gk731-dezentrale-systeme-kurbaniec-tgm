# DEZSYS_GK731_DEZENTRALE_SYSTEME

## Einfuehrung

Seit mehreren Jahren prägen dezentrale Systeme die IT Landschaft von Unternehmen und Haushalten. Die Verbreitung erfolgt oft schleichend und vielen Benutzern ist nicht bewusst, dass sie eine solches komplexes System jeden Tag mehrmals benutzen.

## Voraussetzungen

* Grundlagen Dezentrale Systeme

## Aufgabenstellung

Das Ziel dieser Aufgabenstellung ist es den Begriff dezentrale Systeme und deren Bedeutung im heutigen Alltag eines Unternehmens zu verdeutlichen. Die Teilnehmer des Kurses sollen Recherche zu den vorgetragenen Begriffe durchführen und diese nochmals zusammenfassen.

Weiters soll ein Konzept ausgearbeitet werden, um ein dezentrales System in einer sehr vereinfachten Problemstellung einzusetzen. Hier muss zuerst ein passendes Middleware System gefunden werden und dann sollen die einzelnen Umsetzungsschritte beschrieben werden.

**Problemstellung:**

Zwei Produktionsbetriebe (Mitarbeiteranzahl 50-100 Personen) verwenden jeweils eine eigene Software um Rechnungen/Angebote/Mahnungen zu verwalten. Das Risiko die Rechnungssystem umzustellen wird als zu hoch bewertet, daher wir entschieden, dass ein System benoetigt wird, dass dem jeweils anderen Standort die Suche nach bestimmten Kriterien (Rechnung/Angebot/Mahnung/Kunde) ermöglicht. Sie werden als System Architekt herangezogen, um eine entsprechende "verteilte" Lösung zu konzipieren.

## Aufgabenstellung

Bei der Suche nach den Begriffen & Definitionen vergessen Sie bitte nicht die Quellenangaben anzufuehren.

* Finde eine weitere Definition von "Verteilte Systeme"

Beschreibe die folgenden Begriffe UND formuliere mit eigenen Worten ein Beispiel dazu:

* Skalierbarkeit
* Offenheit
* Transparenz
* Interoperabilitaet
* Portabilitaet
* Replikation
* Migration
* Beschreibung Fehlermodelle
* Lose gekoppelte Systeme
* Service orientierte Architektur
* Nachrichten orientierte Architektur
* Enterprise Architecture Integration
* Cluster
* Gridsysteme
* Cloudsystem
* Dokumente und Links


## Bewertung

Gruppengroesse: 1 Person

### Anforderungen "ueberwiegend erfuellt"

* Definition & Beschreibung der Begriffe
* Grobentwurf/Skizze zur Einführung eines dezentralen Systems zum Austausch von Rechnungsinformationen
* Beschreibung von Gefahren & Risiken des neuen Systems

### Anforderungen "zur Gaenze erfüllt"

* Feinentwurf des Konzepts
* Beschreibung der einzelnen Meilensteine
* Marktübersicht: Welche Systeme/Produkte sind dafür geeignet?
* Entscheidung & Begründung für ein System/Produkt

---
### Ausarbeitung
#### Problemstellung
##### Entwurf
Aus der Aufgabenstellung kann ich herauslesen, dass die Betriebe jeweils eine eigene Software besitzen, mit denen sie ihre Daten (Rechnungen, Angebote, etc.) verwalten. Daraus schließe ich, dass beide über eine Datenbank besitzen, auf die die jeweilige Software zugreift und wahrscheinlich per in der Datenbank gespeicherten Dateipfade die Dokumente holen bzw. herauslesen kann. Deshalb wäre mein Vorschlag, nicht eine universelle Software für beide Betriebe zu entwerfen, sondern, dass die jeweiligen Programme der Firmen auf eine gemeinsame Datenbank in einem Cluster zugreifen. Der Vorteil wäre, dass der "Umbau" des Systems im Hintergrund passieren würde, damit würden Benutzer dies überhaupt nicht merken und müssten sich nicht auf eine neue Software einschulen lassen.

##### Skizze des Systems
![Skizze](Images/Model.png)

##### Risiken
Ein Risiko ist, dass ich nicht weiß welche Datenbankmanagementsysteme die Betriebe benutzen (MySQL, PostgreSQL, MariaDB, ...) bzw. ob sie überhaupt Datenbanken besitzen, oder ob einfach alles nur in freigegeben Ordner gespeichert wird. Beim ersten Fall müsste man alle Datenbanken, die nicht auf der Zieldatenbank basieren, in diese exportiert werden. Dies kann teilweise kritisch sein, denn SQL-Datenbank sollten eigentlich gleich bzw. sehr ähnlich sein, doch jeder Anbieter fügt gerne proprietäre Funktionen hinzu, die diesen Prozess verkomplizieren können. Außerdem besteht immer bei solchen Aktionen das Risiko, dass die Daten beschädigt werden könnten bzw. das Inkonsistenzen auftreten könnten. Deshalb muss vorher ein Backup erstellt werden. Beim zweiten Fall müsste eine Datenbank von Grund auf neu erstellt werden, was viel Zeit beanspruchen könnte, wenn die Daten nicht konsequent abgelegt worden sind.
<br>
Ein weiters Problem in Bezug auf Datenbanken ist, dass beide Betriebe ihre Daten zuerst verbinden müssen, das heißt, dass beide Datenbank in ein logisches System umgewandelt werden. Somit müssen alle Tabellen über die gleichen Attribute verfügen, um eine saubere Verbindung zu gewährleisten.
<br>
Ein weiters Risiko könnte die Kommunikation zwischen dem Produktionsbetrieb A und dem Produktionsbetrieb B sein. Falls keine gute Internet-Infrastruktur vorhanden ist, kann keine Synchronisierung zwischen den Datenbanken der Betriebe stattfinden und dies würde zu Inkonsistenz der Daten führen, was denn Sinn einer Datenbank in Frage stellt.

##### Meilensteine

1. Errichtung einer gemeinsamen Verbindung zwischen Betrieb A und B.
2. Sicherstellung, dass die Kommunikation die Anforderungen erfüllt.
3. Entwurf über gemeinsame Datenbank mit Hilfe eines ERD erstellen.
4. Die vorhanden Datenbanken auf das gemeinsam erstellte Schema überarbeiten.
  1. Falls keine Datenbanken vorhanden ist, diese nach ERD erstellt und Dateipfade speichern.
  2. Falls eine Datenbank verwendet wird, die nicht der Cluster-Datenbank entspricht, muss diese auf die Zieldatenbank ausgerichtet werden.
5. Die jeweilige Firmen-Software auf die neuen Datenbanken ausrichten.
6. Aufbau der Server, Konfiguration der Cluster und Verbindung bzw. Importieren der vorhanden Datenbanken.
7. Überprüfung der Synchronisation und Funktionalität der beiden Firmen-Software.

##### Marktübersicht

* MySQL Cluster <br>
  Ermöglicht die Installierung des gleichnamigen Datenbankmanagementsystems MySQL auf einem Computercluster. Ein Vorteil von MySQL Cluster ist, dass die einzelnen Knoten zur Datensicherung über kein Shared-Memory verfügen müssen. Somit können diese auf einfachen Desktop-Rechner benutzt werden. Für den Cluster gibt es drei Knoten-Arten, wobei zwei mir wichtig erscheinen:
    * Datenknoten <br>
      Speichern die Daten selber. Im jedem Cluster übernimmt genau ein Knoten die Rolle des Masters, der die primären Informationen über den Zustand des Clusters verwaltet. Falls dieser Knoten ausfällt, wird ein anderer zum Master ernannt.
    * Managementknoten <br>
      Sind für die Systemkonfiguration und Knotenverwaltung zuständig. Es können mehrere dieser Knoten zur Ausfallsicherheit erstellt werden.
      <br>
      Mehr: [Link1](https://dev.mysql.com/downloads/cluster/), [Link2](https://de.wikipedia.org/wiki/MySQL_Cluster)
* Oracle RAC <br>
  Oracle Real Application Cluster, ist eine Möglichkeit das Datenbankmanagementsystems von Oracle auf einem Cluster laufen zu lassen. Der Cluster besteht aus mehreren Knoten in einem Rechnerverbund, die über einen gemeinsamen Speicher und dieselbe Datenbank verfügen. Damit kann ein geringer Zeitaufwand für den Wiederanlauf bei Ausfall eines Knotens erreicht werden, doch bei Beschädigung des gemeinsamen Speichern fällt das komplette System aus. Somit muss, damit eine komplette Sicherung implementiert wird, ein zweiter Rechnerverbund aufgebaut werden.  <br>
  Mehr: [Link1](https://www.oracle.com/technetwork/database/options/clustering/overview/index-086583.html), [Link2](https://de.wikipedia.org/wiki/Oracle_RAC#Vor-_und_Nachteile)

##### Begründung
Persönlich würde ich das System mit MySQL Cluster realisieren. Dies hat mehrere Gründe. MySQL ist die populäre Datenbank, hat damit eine größere Dokumentation. Dazu ist sie noch Open-Source und im Grunde kostenlos. Was aber meiner Meinung wichtiger ist, dass beim MySQL Cluster die Knoten über kein Shared-Memory verfügen müssen. Dies ist bei Realisierung zweier Datenbanken in zwei Betrieben von großer Bedeutung. Außerdem, so glaube ich, kann ich dann auch einfach die einzelnen Datenbanken der Firmen in einem Raid-1, also Spiegelung der Daten auf einem zweiten Speicher, noch sicherer machen. Noch dazu habe ich ziemlich schnell eine vereinfachte [Anleitung](https://www.digitalocean.com/community/tutorials/how-to-create-a-multi-node-mysql-cluster-on-ubuntu-18-04) zur Implementierung gefunden. 

#### Begriffe
<!--
Meine Beispiele zu den Begriffen werden sich oft auf mein derzeitiges ITP-Projekt beziehen, wo mein Projektteam und ich Kartenleser zur Anwesenheitskontrolle umsetzten sollen. Diese müssen mit einem Zwischen-Server kommunizieren, der wiederum mit den Schulserver arbeiten muss. Das Projekt heißt Lernbüroverwaltungstool, kurz LBVT.-->

* **Verteilte Systeme** <br>
 <!--Ein verteiltes System ist ein Verbund einer Anzahl mehrerer Computer, die dem Endbenutzer als ein einziges System erscheinen.-->
 Ein verteiltes System beschreibt ein System, in dem viele einzelne, oft unterschiedliche Computer miteinander Arbeiten, um ein gemeinsames Ziel zu erreichen.
  <br>
 [Quelle](https://www4.informatik.uni-erlangen.de/DE/Lehre/WS03/V_VA/Skript/VA-2.pdf), aufgerufen am 16.10.2018
* **Skalierbarkeit** <br>
  Ein System ist skalierbar in Bezug auf Größe, wenn ohne Probleme mehr Nutzern und Ressourcen dem System hinzugefügt werden können.
  <br>
  Bsp: Ein Webserver ist dann skalierbar, wenn er mehr Anfragen auf einmal bearbeiten kann, wenn ich seine Hardware aufrüste. Das Aufrüsten kann auf zwei Arten passieren:
    * "Scale up" - Bestehende Hardware wird durch neue ersetzt (z.B. neue CPU)
    * "Scale out" - Bestehende Hardware wird ergänzt (z.B. zweite CPU)
  <br>
  Quelle: Buch, Seite 26
* **Offenheit**
  <br>
  Ein offenes verteiltes System, ist ein System, welche Dienste nach standardisierten Regeln anbietet, welche die Syntax und Semantik der Dienste beschreiben.
  <br>
  Bsp: Wichtig bei Offenheit ist, dass unterschiedliche Systeme miteinander kommunizieren können, dazu muss jede Partei gewisse Standards befolgen. Wenn ich mit einem meinem Client eine Email verschicke, mit einem Übertragungsprotokollen wie z.B. POP3 oder IMAP, muss der Email-Server diesen auch beherrschen und auch der Empfänger-Client muss damit wieder arbeiten können.
  <br>
  Quelle: Buch, Seite 24
* **Transparenz** <br>
  Eine wichtige Eigenschaft von verteilten System ist, dass die Ressourcen, die auf mehreren verschiedenen Computer physisch verteilt sind. Ein verteiltes System, dass sich dem Benutzer erfolgreich als ein einziger Computer präsentiert, wird als transparent bezeichnet.
  <br>
  Bsp: Wenn ich mir ein Video auf einer großen Videoplattform, wie z.B. YouTube, anschaue sehe ich nicht welcher Server mir die Videodatei anbieten und wie viele als Backup fungieren. Als Benutzer erscheint mir alles als ein großes System.
  <br>
	Quelle: Buch, Seite 21
* **Interoperabilität** <br>
  Beschreibt wie zwei Systeme oder Komponenten von verschiedenen Herstellern miteinander harmonieren und zusammen arbeiten können, wobei man lediglich Dienste verwenden, die einen gemeinsamen Standard besitzen.
  <br>
  Bsp: Ich kann zwei Router, von verschiedenen
  Herstellern, miteinander verbinden und alle werden den selben Zweck erfüllen und miteinander kommunizieren können.
  <br>
  Quelle: Buch, Seite 25
* **Portabilität** <br>
  Gibt an wie gut eine Applikation für ein verteiltes System A ohne Veränderung auf einem verteilten System B mit gleichen Schnittstellen ausgeführt werden kann.
  <br>
  <!--
  Bsp: Im Falle unser Projektes, wäre es interessant zu wissen, wie gut die Software unserer Kartenleser(die auf Mikrocontrollern basieren) auf anderen Mikrocontrollern funktionieren würden. Damit kann die Portabilität beurteilt werden.-->
  Bsp: Wenn ich eine Applikation, die für Windows ausgelegt ist, auf einen anderen Windows-Computer ausführe, ist die wahrscheinlich sehr hoch, dass diese dort ohne Probleme funktioniert, wenn die Windows-Versionen einigermaßen ident sind. Wenn ich die Applikation auf anderen Betriebssystemen wie macOS oder verschiedenster Linux-Distributionen ausführen möchte, ist dies kein leichtes unterfangen, da diese andere Ausführungstypen besitzen, sowie einen komplett anderen Betriebssystem-Kernel. Auf diese müsste die Applikation "portiert" werden, also auf diese Betriebssysteme angepasst werden.
  <br>
  Quelle: Buch, Seite 25
* **Replikation** <br>
  Bezeichnet das Ressourcen auf verschiedenen Servern als Kopien abgespeichert werden, um beispielsweise die Zugriffszeiten zu verkürzen oder die Verfügbarkeit dieser Ressource zu erhöhen.
  <br>
  Bsp: Hier würde ich wieder das Beispiel mit der Videoplattform wählen. Wenn ich mir was auf YouTube anschaue, wird mir das Video von einem Server angeboten, der wahrscheinlich in meiner Nähe liegt und nicht z.B. in Amerika, damit ich "schneller" das Video laden kann (schneller heißt eigentlich niedrigere Latenz, bzw. wenigere verloren Datenpakete).
  <br>
  Quelle: Buch, Seite 22
* **Migration** <br>
  Gibt an das Ressourcen an einen anderen Ort verlegt werden können. Wichtig ist aber, dass der Zugriff auf die Ressource dadurch sich nicht ändert.
  <br>
  Bsp: Ich hoste privat einen eigenen Webserver mit der URL: http://www.toller_webserver. Ich wechsle meinen Standort und verlagere dort auch meinen Webserver. Jeder hat immer noch Zugriff auf den Webserver mit der gleichen URL.
  <br>
  Quelle: Buch, Seite 21/22
* **Beschreibung Fehlermodelle** <br>
  <!--
  * fail-halt (fail-stop) system: <br>Der Server stürzt ab und teilt das entweder vorher mit oder es ist für andere, von ihm abhängige Prozesse ersichtlich, dass er abgestürzt ist.
  * fail-passive (fail-silent) system: <br>Server stürzt ab, aber teilt dies nicht mit (passiert häufig), hier ist es schwierig festzustellen, ob der Server tatsächlich down oder nur langsam ist (oder das Problem ganz woanders liegt, etwa in der Netzwerkverbindung).
  * fail-consistent system: <br>Kein Byzantinischer Fehler, also immmer gleicher bzw. ähnlicher Fehler.
  * fail-inconsistent system: <br>Jegliche Art von Fehler.
  * fail-safe system: <br>Output des Systems ist falsch, kann aber von anderen Prozessen als solcher erkannt werden.
  <br>
  <a href="https://vowi.fsinf.at/wiki/TU_Wien:Verteilte_Systeme_VO_(G%C3%B6schka)/Fragenkatalog_Wiki#Fehlermodelle_und_k-fault-tolerance">Quelle</a>, aufgerufen am 05.11.2018-->
  * Absturzausfall (Crash Failure) <br>
    Ein Server steht, hat aber bis dahin richtig gearbeitet. Der angebotene Dienst bleibt beständig aus (ständiger Dienstausfall)
  * Dienstausfall (Omission Failure) (Empfangsauslassung, Sendeauslassung)<br>
    Ein Server antwortet nicht auf eingehende Anforderungen. Ein Server erhält keine eingehenden Anforderungen. Ein Server sendet keine Nachrichten.
  * Zeitbedingter Ausfall (Timing Failure)<br>
    Die Antwortszeit eines Servers liegt außerhalb bei festgelegten Zeitintervalls.
  * Ausfall korrekter Antwort (Response Failure)<br>
    Die Antwort eines Servers ist falsch. Dieser Ausfall wird oft auch kurz Antwortfehler genannt.
    * Ausfall durch Wertefehler (Value Failure)<br>
      Der Wert der Antwort ist falsch.
    * Ausfall durch Zustandsübergangsfehler (State Transition Failure)<br>
      Der Server weicht vom richtigen Programmablauf ab.
  * Byzantinischer oder zufälliger Fehler (Arbitrary oder Byzantine Failure)<br>
    Ein Server erstellt zufällige Antworten zu zufälligen Zeiten.
  ---
  * Aufsfallsichere Fehler (fail-safe system): <br>Output des Systems ist falsch, kann aber von anderen Prozessen als solcher erkannt werden.
  <br>
  * Stiller Ausfall-Fehler (fail-passive (fail-silent) system): <br>Server stürzt ab, aber teilt dies nicht mit (passiert häufig), hier ist es schwierig festzustellen, ob der Server tatsächlich down oder nur langsam ist (oder das Problem ganz woanders liegt, etwa in der Netzwerkverbindung).
  * Ausfall-Stopp Fehler (fail-halt (fail-stop) system): <br>Der Server stürzt ab und teilt das entweder vorher mit oder es ist für andere, von ihm abhängige Prozesse ersichtlich, dass er abgestürzt ist.
  <br>
  <a
  href="https://vowi.fsinf.at/wiki/TU_Wien:Verteilte_Systeme_VO_(G%C3%B6schka)/Fragenkatalog_Wiki#Fehlermodelle_und_k-fault-tolerance">1.Quelle</a>, aufgerufen am 05.11.2018    <br>
 2.Quelle 2.Buch, Seite 357
* **Lose gekoppelte Systeme**<br>
  Bezeichnet das Computer oder Komponenten in einem verteilten System untereinander lose gekoppelt sind, also keine große Abhängigkeit Zwischen ihnen besteht. Dadurch können die einzelnen Computer leichter durch andere ersetzt werden.
  <br><!--
  Bsp: Ich beziehe mich wieder auf das ITP-Projekt. Falls das System leicht andere Kartenleser zulässt, also nicht vorgesehene Modelle mit leichter Modifizierung unterstützt, besitzt es in diesem Punkt eine lose Kopplung.-->
  Bsp: Man könnte sagen, dass Mail-Dienste lose gekoppelt sind, denn das einzige was eingehalten werden muss sind die Mail-Protokolle wie POP3 oder IMAP. Es spielt keine Rolle welche Architektur der Client hat, ob es ein Smartphone oder normaler Rechner ist, hat keine Auswirkungen.
  <br>
  [Quelle](https://de.wikipedia.org/wiki/Lose_Kopplung), aufgerufen am 05.11.2018
* **Nachrichten orientierte Architektur** (Message Oriented Middleware) <br>
  Beruht auf der Übertragung von Nachrichten zwischen verschiedenen Systemen (meist Client und Server) mittels synchroner oder asynchroner Kommunikation. Das Format der Nachrichten ist nicht geregelt, es werden meist XML oder JSON verwendet.
  <br>
  <!--Über Nachrichten-Broker, Austausch über Nachrichten-->
  Bsp: Eine Beispiel für ein Produkt für Message Oriented Middleware wäre IBM MQ, dieses erlaubt Programmen in einem verteilten System mit einander sicher zu kommunizieren. MQ besteht aus drei Hauptkomponenten: Messages (Nachrichten), Queues (Warteschlange) und Manager (regelt Ablauf). Das Produkt ist für verschiedenste Betriebssysteme ausgerichtet und unterstützt viele APIs verschiedenster Programmiersprachen.
  <br>
  [Quelle](https://de.wikipedia.org/wiki/Message_Oriented_Middleware), aufgerufen am 20.11.2018
* **Service orientierte Architektur** <br>
  Viele verschiedene, einzelne Systeme, die über einem Enterprise Service Bus gemeinsam kommunizieren können.
  <br>
  Bsp: In einer Schule gibt es viele verschiedene Systeme (Stundenpläne, Elearning, Schul-Homepage), diese unterscheiden sich stark von einander (Architektur, Betriebssystem, Benutzte Programmiersprache), dadurch brauchen sie eine gemeinsame Schnittstelle.
  <br>
  [Quelle](https://en.wikipedia.org/wiki/Service-oriented_architecture), aufgerufen am 23.11.2018
* **Enterprise Architecture Integration** <br>
  Wenn man zwei Unternehmen miteinander verbinden will, benutzt man ein Business Process Management (BPM) um die Kommunikation zwischen den beiden Parteien zu ermöglichen. Ist komplizierter als eine Service orientierte Architektur.
  <br> Bsp: Jetzt kann man ein ähnliche Beispiel wie bei der Service orientierten Architektur nennen, doch anstatt das die Systeme einer einzelnen Schule miteinander arbeiten, weitet man das Konzept aus, dass mehrere Schulen miteinander arbeiten und das die System dieser mit den anderen kommunizieren können.
  <br>
   [Quelle](https://de.wikipedia.org/wiki/Enterprise_Application_Integration), aufgerufen am 23.11.2018
* **Cluster** <br>
  Ist eine Ansammlung von Computern in einem einen high-Speed-LAN-Netzwerk. In Clustern ist die Hard- und Software der Computer meist ident.
  <br>
  Bsp: Man möchte einen mehr als schnellen Supercomputer bauen, um komplexe Probleme zu lösen. Da einzelne Computer zu schwach für die Aufgabenstellungen sind, werden mehrere Computer zusammen verbunden, um ein Cluster, und damit einen Supercomputer zu bilden.
  <br>
  1.Quelle: Buch, Seite 35 <br>
  [2.Quelle](https://stackoverflow.com/questions/9723040/what-is-the-difference-between-cloud-grid-and-cluster), aufgerufen am 05.11.2018
* **Gridsystem**<br>
  Ist eine Ansammlung von inhomogenen Computern, also die einzelnen Computer besitzen oft unterschiedliche Hard- und Software. Meistens werden bei Grids Ressourcen von verschiedenen Organisationen zusammengebracht, um die Zusammenarbeit von mehreren Personengruppen oder Organisationen zu realisieren. <br>
  Bsp: Wenn mehrere wissenschaftliche Institute miteinander kooperieren möchten, werden sie wahrscheinlich auf Gridsysteme zugreifen, um Ressourcen wie Rechenleistung untereinander anbieten zu können.
  <br>
  1.Quelle: Buch, Seite 35 <br>
  [2.Quelle](https://stackoverflow.com/questions/9723040/what-is-the-difference-between-cloud-grid-and-cluster), aufgerufen am 05.11.2018
* **Cloudsystem** <br>
  Beschreibt das Anbieten von Ressourcen über das Internet. Cloud-basierte Anwendungen und Software laufen auf den Computern von Cloudsystem und werden meist beim Client im Browser angezeigt, das heißt der Client muss sie nicht lokal speichern bzw. ausführen. Im Vergleich zu Grids werden Cloudsysteme meistens von einer Organisation geführt.
  <br>
  Bsp: Um meine Daten überall mitzuhaben, kann ich diese bei einem Cloudsystem-Anbieter hochladen, der Netzwerkspeicher zur Verfügung stellt.
  <br>
  [1.Quelle](https://www.ibm.com/cloud/learn/what-is-cloud-computing), aufgerufen am 05.11.2018 <br>
  [2.Quelle](https://stackoverflow.com/questions/9723040/what-is-the-difference-between-cloud-grid-and-cluster), aufgerufen am 05.11.2018
<!--* Dokumente und Links-->


### Literaturverzeichnis
Buch *Title* Distributed Systems, Principles and Paradigms, Second edition, Adjusted for digital publishing, *Author* Andrew S. Tanenbaum, Maarten van Steen, *Year* 2007, *Book*, *Retrieved from URL:* https://www.distributed-systems.net/index.php/books/distributed-systems/ <br>
2.Buch *Title* Verteilte Systeme, Prinzipien und Paradigmen, 2., aktualisierte Auflage, *Author* Andrew S. Tannenbaum, Maarten van Steen, *Year* 2008, *Book*, *Publisher* Pearson Studium
